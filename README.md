# Dezentrale Server

## Dienste

- Dokumentenablage -> Nextcloud
- Kalender -> Nextcloud
- Matrix-Client -> Nextcloud App, oder etwas anderes?
- Kanban -> taiga
- Git -> Gitlab
- Konferenz > Jitsi
- Mail -> GUI über Nextcloud, Implementierung?
- Wiki -> alte Mediawiki, Markdown über Nextcloud/Git
- Projekt-Space (für eigene Projekte der Member)
- Dezentrale-Schlüsseldienst
- Web Space (Dezentrale Seite, Statische Inhalte)

## Grundsätzliches

Installation von Ansible

```
# Debian/Ubuntu
apt install python3 python3-pip
pip3 --user install ansible
```

Ausführen von Ansible

```
ansible-playbook -i hosts.yml --extra-vars="ansible_ssh_private_key_file=<SSH-KEY>"
```

## Playbooks

- **user-setup.yml** Alle Basisaccounts (manuelle User, Deployment User) einrichten sowie notwendige Programme installieren
- **jump-host.yml** Einrichtung des Jump Host (Zz. nicht genutzt)

### user-setup.yml

Es wird *sudo* und *python3* soweit es nicht installiert ist, ein User *deploy* angelegt, ihm *sudo*-Rechte gegeben und SSH-Keys hinterlegt. Die Keys werden über die User Datenbank *vars/users.yml* ausgelöst.

Es können Standard deployment keys in der Variable *deployment_ssh_keys* hinterlegt werden 

```yaml
deployment_ssh_keys:
- name: <Freiwählbarer Schlüsselname>
  key: <SSH Public Key> 
```

Bei der Ausführung werden sämtliche bereits vorhandenen Keys überschrieben.

### Aufbau der User Datenbank

Pro Dictonary Eintrag gilt:

```yaml
<USERNAME>:
    name: <DISPLAY NAME>    # (Optional)
    ssh_keys:
        - <KEY 1>           # (Notwendig)
        - <KEY 2>           # (Optional)
        - ...
    sudo: <yes|no>          # (Optional, Standard no)
    deploy: <yes|no>        # (Optional, Standard no)
```

### jump-host.yml

Es wird ein Nutzer *jump* eingerichtet und alle SSH Deployment Keys hinterlegt. Zugelassene Accounts können nur ein Port Forwarding nutzen. Eine Nutzung der Shell ist nicht möglich.

### setup-vms.yml

Erzeugt VMs nach der Defintions in hosts.yml aus der Grupp *virtual_machines*.
